#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main (int argc, char const *argv[]) {
  FILE * pFile; //input file
  FILE * outFile;//output file for threshold filter
  FILE * flipFile;//output file for fliped picture
  FILE * rotateFile;//oupput file for rotated picture
  long fSize;
  unsigned char * buffer;
  unsigned char * outbuffer;
  unsigned char * flipbuffer;
  unsigned char * rotatebuffer;
  size_t result;
  char filename[30];
  char outputName[30] = "out.bmp";
  float threshold;
  int rotate;
  char flipDirection[30];
  char thre[30];


  
  //to get the output name,input name and threshold
  strcpy(filename, argv[argc-1]);
  for (int i = 0; i < argc; ++i)
  {
    if (strcmp(argv[i],"-t")==0){ 
      sscanf(argv[i+1],"%f",&threshold);
      strcpy(thre, "thre");
    }
    else if (strcmp(argv[i],"-o")==0)
    {
      strcpy(outputName, argv[i+1]);
    }else if (strcmp(argv[i],"-r")==0)
    {
     sscanf(argv[i+1],"%d",&rotate);
    }else if (strcmp(argv[i],"-f")==0)
    {
      strcpy(flipDirection, argv[i+1]);
    }
    else if (strcmp(argv[i],"-h")==0)
    {printf("Usage: bmpedit [OPTIONS...] [input.bmp]\n"
            "DESCRIPTION:\n"
            "This program does simple edits of BMP image files. When the program runs it first prints\n"
            "out the width and the height of the input image within the BMP file.  Once this is done if\n"
            "there is a filter (or sequence of filters) then they are applied to the image.  The resulting image is also \n"
            "stored using BMP format into an output file.  Without any filters only the width and height of the image is output.\n"
            "OPTIONS:\n"
            "-o FILE      Sets the output file for modified images (default output file is out.bmp).\n"
            "-t 0.0-1.0   Apply a threshold filter to the image with a threshold the threshold value given.\n"
            " -h           Displays this usage message.\n"
            " -r 180 or 270  rotate the picture by 180 degree or 270 degree.\n"
            "-f horizontal flip the picture horizontally");
     }
   }

//open file
  pFile = fopen (filename, "rb" );
  if (pFile==NULL){
    fputs ("File error",stderr); 
    exit (1);
  }

  // obtain file size:
  fseek(pFile , 0 , SEEK_END);
  fSize = ftell (pFile);
  rewind (pFile);

  // allocate memory to contain the whole file:
  buffer = (unsigned char*) malloc (sizeof(char)*fSize);
  if (buffer == NULL) {
    fputs ("Memory error",stderr); 
    exit (2);
  }

  // copy the file into the buffer:
  result = fread (buffer,1,fSize,pFile);
  if (result != fSize) {
    fputs ("Reading error",stderr);
     exit (3);
   }

  /* the whole file is now loaded in the memory buffer. */
 //print out the length and width

  int width = (buffer[21]<<24) + (buffer[20]<<16) + (buffer[19]<<8) + buffer[18];
  int height = (buffer[25]<<24)+(buffer[24]<<16)+(buffer[23]<<8) + buffer[22];
  printf("Image width: %d \n", width );
  printf("Image height: %d \n", height );

  //add threshold filter
 
  int rowSize = width * 3;
    int pad = 0; // Set pad byte count per row to zero by default.
  // Each row needs to be a multiple of 4 bytes.  
  if ((rowSize) % 4 != 0) {
    pad = 4 - ((rowSize) % 4);
  } // 4 - remainder(width * 3 / 4).
  if (strcmp(thre,"thre")==0)
  {
      outbuffer = (unsigned char*) malloc (sizeof(char)*fSize);
  if (outbuffer == NULL) {
    fputs ("Memory error",stderr); 
    exit (4);
  }//copy the file to the outbuffer for future change
  int y;
  for (y= 0; y < 54; ++y)
  {
    outbuffer[y]=buffer[y];
  }
    int i;
    int j = 0;
    char color;
    float narrow = 3 * 255;
  
  for (; j < height; ++j) { 
     int paddingRow = (rowSize + pad) *j;

    for (i = 0 ;i < rowSize ; i += 3){
  
      float average=(buffer[(54+i) + paddingRow]
        +(buffer[(55+i) + paddingRow])
        +(buffer[(56+i) + paddingRow])) / narrow;

      color = average >= threshold ? 0xFF : 0;

      outbuffer[(54+i) + paddingRow]
        = outbuffer[(55+i) + paddingRow]
        = outbuffer[(56+i) + paddingRow]
        = color;
    }
    for (int padval = 0; i < pad; ++i)
    {
      outbuffer[54+rowSize+padval+paddingRow]=0;
    }
  }

  outFile = fopen (outputName, "wb");
  fwrite(outbuffer , 1, fSize, outFile);
  fclose(outFile);
  free(outbuffer);
}


  //flip the picture horizontally
  if (strcmp(flipDirection,"horizontal")==0)
  {
  flipbuffer = (unsigned char*) malloc (sizeof(char)*fSize);
  if (flipbuffer == NULL) {
    fputs ("Memory error",stderr); 
    exit (5);
  }
  
  int yi;
  for (yi= 0; yi < 54; ++yi)
  {
    flipbuffer[yi]=buffer[yi];
  }
  
  int w;
  int h=0;
  for (; h < height; h++){
    int paddingRow = (rowSize + pad) *h;
    for (w= 0; w < rowSize; w+=3){
      flipbuffer[(54+w) + paddingRow]=buffer[54+rowSize-w-3+paddingRow];
      flipbuffer[(55+w) + paddingRow]=buffer[54+rowSize-w-2+paddingRow];
      flipbuffer[(56+w) + paddingRow]=buffer[54+rowSize-w-1+paddingRow];
      
    }
    for (int ipad = 0; ipad < pad; ++ipad)
    {
      flipbuffer[54+rowSize+ipad+paddingRow]=0;
    }
  }

  flipFile = fopen (outputName, "wb");
  fwrite(flipbuffer , 1, fSize, flipFile);
  
   //terminate
  fclose(flipFile);
  free(flipbuffer);

 }

  //rotate the picture by 180 degree or 270 degree
if (rotate==180||rotate==270)
{
 
  rotatebuffer = (unsigned char*) malloc (sizeof(char)*fSize);
  if (rotatebuffer == NULL) {
    fputs ("Memory error",stderr); 
    exit (6);
  }
  
  int yii;
  for (yii= 0; yii < 54; ++yii){
    rotatebuffer[yii]=buffer[yii];
  }
  
  if (rotate == 270){ 

    int rwidth = height;
    int rheight = width;
    int rowS = rwidth * 3;
    //change the header information about width and height
    for (int ind = 0; ind < 4; ++ind){
      rotatebuffer[18+ind]=buffer[22+ind];
      rotatebuffer[22+ind]=buffer[18+ind];
    }

    int padr=0;
    if ((rowS) % 4 != 0)
    {
      padr=4 - (rowS) % 4 ;
    }

    int hei=0;
    int wid;
    for (; hei < height; ++hei){
      for (wid = 0; wid < rowSize; wid+=3){
        int paddingRow=(rowSize+pad)* hei;
        int paddingRowR=(rowS+padr) * wid/3;
        rotatebuffer[54+rowS+paddingRowR-3-3*hei]=buffer[54+wid+paddingRow];
        rotatebuffer[54+rowS+paddingRowR-2-3*hei]=buffer[55+wid+paddingRow];
        rotatebuffer[54+rowS+paddingRowR-1-3*hei]=buffer[56+wid+paddingRow];

      }
    }
    int hr=0;
    
    for (; hr < rheight; ++hr)
    { int paddingRow=(rowS+padr)*hr;
      for (int ipad=0;ipad<padr;ipad++)
      {
        rotatebuffer[54+rowS+ipad+paddingRow]=0;
      }
    }
 
  }else if (rotate==180){
    int wr;
    int hr=0;
    for (; hr < height; hr++){
      int paddingRow = (rowSize + pad) *hr;
      for (wr= 0; wr < rowSize; wr+=3){
      
        rotatebuffer[(54+wr) + paddingRow]=buffer[54+rowSize-wr-3+(rowSize + pad) *(height-1-hr)];
        rotatebuffer[(55+wr) + paddingRow]=buffer[54+rowSize-wr-2+(rowSize + pad) *(height-1-hr)];
        rotatebuffer[(56+wr) + paddingRow]=buffer[54+rowSize-wr-1+(rowSize + pad) *(height-1-hr)];
      }
      for (int ipad = 0; ipad < pad; ++ipad){
        rotatebuffer[54+rowSize+ipad+paddingRow]=0;
      }
    }
  }

  rotateFile = fopen (outputName, "wb");
  fwrite(rotatebuffer , 1, fSize, rotateFile);
  fclose(rotateFile);
  free(rotatebuffer);
}
  //terminate
  
  fclose (pFile);
  free (buffer);
  return 0;
}





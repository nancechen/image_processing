
*Name : Ke Chen <br/>

<br/>

##summary <br/>
The program read the bmp file, copy it to a buffer and do some change according to the command. The basic parts that the program can do are reading the width and height imformation. Some advanced functionalities that the program has are flipping the image horizontally and rotating the image by 180 degree or 270 degree.
<br/>

##how to compile and run <br/>
The program can be compiled by simpily using:<br/>
"make" or "gcc -Wall -o bmpedit bmpedit.c"<br/>
Applying the threshold to the bmp file:<br/>
"-t 0.0-1.0 -o outputFile cup.bmp" in the command shell. <br/>
Applying the rotation:<br/>
"-r 180 or 270  -o outputFile cup.bmp"<br/>
Flipping the image horizontally:<br/>
"-f horizontal -o outputFile cup.bmp"<br/>
Simply read the height and width information:<br/>
"./bmpedit cup.bmp"<br/>
Help information:<br/>
"./bmpedit -h"<br/>
<br/>


##test<br/>
Test on the cropped cup.bmp file ("cup_small.bmp") <br/>
Test with different threhold values<br/>
Test with no "-o outputFile" in the command<br/>
<br/>

##extension <br/>
The extensions I did are the following:<br/>
rotate the picture by 180 degree and 270 degree<br/>
flip the picture horizontally<br/>
<br/>

##limitation <br/>
There is limitation in choosing the rotation angle. So far, there is only 180 degree and 270 degree available. Also, the image can only be flipped horizontally.



